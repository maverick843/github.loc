// import 'babel-polyfill';
import githubScript from '../page_scripts/github';

$(function () {

	if ($('#github').length) {
		githubScript(window, document, jQuery);
	}

});