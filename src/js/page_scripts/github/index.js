import Vue from 'vue';
import App from './App.vue';
import store from './store/index';

export default function (window, document) {

	new Vue({
		el: '#github',
		render: h => h(App),
		store
	});

}