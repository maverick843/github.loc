import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        search: '',
        result: {}
    },
    getters: {
        getResult: (state) => state.result
    },
    mutations: {
        updateSearch: (state, payload) => {
            state.search = payload;
            console.log('state.search =', state.search);
        },
        updateResult: (state) => {
            $.ajax({
                'url': `https://api.github.com/search/repositories?q=${state.search}`,
                dataType: 'json',
                success: function(resp){
                    state.result = resp.items;
                    console.log(state.result);
                }
            });
        }
    },
    actions: {
        update: (context, payload) => {
            context.commit('updateSearch', payload);
            context.commit('updateResult');
        },
    }
})